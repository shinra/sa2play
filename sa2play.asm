;
;  Player Sadt (Adlib)
;
;  Jump en #8000 apres avoir charge .sa2 a l'adresse 'file' 
;
;  v11 12/11/2020
;     Adapted by PulkoMandy for use with WILLY + OPL3LPT instead of CPC-ISA
;     (all previous work by Madram)
;
;  v10 27 7 2002
;       . Tentative de gestion d'option "break pattern"
;	  pas tr}s concluant.
;         
;
;  v9  12 8 2000   Non testee !!  	
;       . Correction bug. On n'envoie plus note quand elle est a 0.
;       . Aiguillage routines effets.
;       . Split des variables 16 bits (cf segments...)
;
; Attention  	. Delay et position dans pattern traites globalement
;		  (contrairement au player original)
;
; NB  	 Dans player original, work_pattern contient (indirectement)
;	 l'adr de debut de piste, et l'adr a lire est calculee d'apres
;	 current_line.
;	 Ici, on stocke l'adr a lire dans 'segments', et on l'incremente.
;        

	org #8000
;	write"sa2play.cod"

;
; . J'adore ce son !
; . Il n'est pas mauvais, mais je prefere l'avoine.
;	
	nolist

file	equ #4000
	
FM	equ #FE84

; 
; On joue la zic. Personne n'est capable de taper 'call play' a 50hz 
;
	call Init : ret c
;
testloop
	call waitvbl : call antivbl : call Play
        call #bb09 : jr nc,testloop

	call StopSound : jp Fin


;
; Offsets du header sadt 
;

instruments	equ 5
patternorder   	equ 966
songlength     	equ 1096
songloop	equ 1097
restart        	equ 1097
bpm            	equ 1098
arpeggio       	equ 1100
acommands      	equ 1356
trackorder     	equ 1612
channels       	equ 2188
trackdata      	equ 2190


Init	
; Init variables

	ld hl,defaut
	ld de,variable
	ld bc,defaut-variable
	ldir
            
	ld a,(file+songlength)
	ld (song_length),a
	ld a,(file+songloop)
	ld (song_loop),a

	xor a
	call init_segments

; Registres generaux

        ld a,#01 : ld l,#20 : call Setreg
        ld a,#08 : ld l,#40 : call Setreg
        ld a,#BD : ld l,#C0 : call Setreg

	or a		; Carry a 0 = init ok
	jp Fin
	
Fin	
	ret

xca equ channel_active-X
xcn equ channel_note-X
xcfl equ channel_freq.l-X
xcfh equ channel_freq.h-X
xci equ channel_ins-X
ysl equ segments.l - Y
ysh equ segments.h - Y

Play

; Nouvelle ligne ?

	ld a,(d_count) : or a : jr nz,effects

	ld ix,X : ld iy,Y
	ld bc,#900
Pl_lp
	push bc
;
	ld a,(IX+xca)
	or a
	jr z,do_nothing

;
; On lit donnees fraiches
; 

	ld l,(IY+ysl)		;adr pattern
	ld h,(IY+ysh)
	ld a,(hl)			;Note
        and #fe				;On ecarte bit instrument
	ld (IX+xcn),a	;On stocke pour effets
	jr z,do_nothing

; Conversion note frequence

	ld c,a			;Bits bien places !
	ld b,0
 	ld de,NoteTab          
	ex de,hl
	add hl,bc
	ld a,(hl)		
        ld (IX+xcfl),a
	inc hl
	ld a,(hl)
	ld (IX+xcfh),a
	ex de,hl
;
; On isole instrument
;
	ld e,(hl) : inc hl : ld a,(hl)
	rr e : rra : srl a : srl a : srl a
	jr nz,ins_found
	ld a,(IX+xci) ;on recupere no ins precedent
ins_found	
	ld (IX+xci),a
	ld de,15:call Multi	  ;Sacre Gilles
	ld bc,file+instruments-15
	add hl,bc
        pop bc 			;Recupere canal
	push bc			
        call SetINS		;On envoie direct

	call Send_SND	
do_nothing
	pop bc

	inc ix : inc iy
	inc c
	djnz Pl_lp


effects

	ld ix,X : ld iy,Y
	ld bc,#900
Ef_lp
	push bc
;
	ld a,(IX+xca)
	or a
	jr z,ef.do_nothing

;
; Meme demarche que precedemment
; 

	ld l,(IY+ysl)		;adr pattern
	ld h,(IY+ysh)
	inc hl				;on saute note
	ld a,(hl) : inc hl
        and #f : rlca : jr z,ef.do_nothing

	ld b,0 : ld c,a
	ld a,(hl) : inc hl		;parametre 			
        push hl : ld hl,effect_calls : add hl,bc
	ld e,(hl) : inc hl : ld d,(hl)
	ld bc,effect.ret : push bc
	ex de,hl : jp (hl)

effect.ret
	pop hl

ef.do_nothing
	pop bc

	inc ix : inc iy
	inc c
	djnz Ef_lp

;
; Avancee de la pattern
;

	ld a,(d_count)
	or a
	jr z,avancee

	dec a : ld (d_count),a
	ret

avancee
	ld a,(delay) : ld (d_count),a
	
	ld a,(current_line0)
	inc a : and 63
	ld (current_line0),a
	jr z,Next_Ptn

	ld hl,segments.l
	ld de,segments.h
	ld a,9
avanc_lp
	ld c,(hl) 
	ex de,hl
 	ld b,(hl)  
	inc bc : inc bc : inc bc
	ld (hl),b : inc hl
	ex de,hl
	ld (hl),c : inc hl

	dec a
	jr nz,avanc_lp
	ret

Next_Ptn
	ld a,(song_pos)
	inc a
	ld hl,song_length
	cp (hl)
	jr nz,NP_ok
	ld a,(song_loop)
NP_ok
	ld (song_pos),a

init_segments
	
	ld b,0
	ld c,a
	ld hl,file+patternorder
	add hl,bc
	ld a,(hl)

	ld h,b	  ; on multiplie par neuf
	ld l,a
	ld c,a
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,bc
	
	ld bc,file+trackorder
	add hl,bc
	ex de,hl

	ld ix,channel_active
	ld iy,segments.l
        ld b,9
	
set_track
	push bc
	ld a,(de) 
	ld (ix),a 	;Pointeur utilise comme Flag 0 ou <>0  
	or a
	jr z,st_noactive ;on ne stocke meme pas d'adr
	
	push de
	ld de,192
	call Multi
	ld de,file+trackdata-192
	add hl,de
	ld (iy+0),l
	ld (iy+9),h
	pop de

st_noactive
	inc iy
        inc ix
        inc de
	pop bc
	djnz set_track

	ret

effect_calls
	dw ef_arpeggio
	dw ef_slide.up
	dw ef_slide.down
	dw ef_portamento
	dw ef_vibrato
	dw ef_protamento.volume_slide
	dw ef_vibrato.volume_slide
	dw ef_empty
	dw ef_release.note
	dw ef_empty
	dw ef_volume_slide
	dw ef_position.jump
	dw ef_set.volume
	dw ef_pattern.break
	dw ef_empty
	dw ef_effect.set_speed

ef_arpeggio
	ret

ef_slide.up
	ret

ef_slide.down
	ret

ef_portamento
	ret

ef_vibrato
	ret

ef_protamento.volume_slide
	ret

ef_vibrato.volume_slide
	ret

ef_empty
	ret

ef_release.note
	ret

ef_volume_slide
	ret

ef_position.jump
	ret

ef_set.volume
	ret

ef_pattern.break
	ld a,62 ; ne g}re pas le param}tre pour l'instant !!!
	ld (current_line0),a
	ret

ef_effect.set_speed
	ret

Send_SND

; Envoie des notes   Entree c = canal

	ld b,(IX+xcfl)
	ld a,#a0		;Frequence (low 8 bits)
	call setreg_type0

	ld b,(IX+xcfh)
	ld a,#b0		;Key On / Octave / Frequence (high 2 bits)
	res 5,b
	call setreg_type0

	ld a,#b0
	set 5,b
	call setreg_type0	

	ret

SetINS
;
; Entree = hl
;
        ld a,#c0		; Feedback / Connection
	call setreg_type1
	
	ld a,#20		; Amp mod / Vibrato / EG type / Key Scaling / Multiple
	call setreg_type2
	ld a,#23		; Idem pour operateur 2
	call setreg_type2

	ld a,#60		; Decay / Attack
	call setreg_type2
	ld a,#63		; Op 2
	call setreg_type2

	ld a,#80		; Sustain / Release
	call setreg_type2
	ld a,#83
	call setreg_type2

	ld a,#E0		; Wave select
	call setreg_type2
	ld a,#E3
	call setreg_type2

	ld a,#40		; Level Key Scaling / Total Level
	call setreg_type2
	ld a,#43
	call setreg_type2

	ret	

Setreg	;Place L dans registre opl designe par A 

	LD BC,FM
	OUT (C),A
	INC C
	OUT (C),L
	;ld b,5			; A regler finement si player devient trop lent
	;call tempo
	ret

setreg_type0	;Place B dans REG + no canal 

	; REG = A+C
	; VAL = B
	PUSH HL
	PUSH BC
	LD L,B
	add a,c

	; REG = A
	; VAL = L

	LD BC,FM
	OUT (C),A
	INC C
	OUT (C),L

	POP BC
	POP HL
	ret


setreg_type1	;Place donnee dans REG + no canal (reg C)

	add a,c
	PUSH BC
	LD BC,FM
	OUT (C),A
	ld a,(hl)
	inc hl
	inc c
	OUT (C),a
	POP BC
	ret

setreg_type2	;Place donnee dans REG (d'apres offsets operateurs)

	push hl
	ld hl,op_offset
	ld b,0
	add hl,bc
	ld b,(hl)
	pop hl

	add a,b

	PUSH BC
	LD BC,FM
	OUT (C),A

	ld a,(hl)
	inc hl
	INC C
	OUT (C),a
	POP BC
	ret




StopSound
	ld b,9
        ld a,#b0
SS_lp
	PUSH BC
	LD BC,FM
	OUT (C),a
	inc C
	inc a
	OUT (C),0
	POP BC
	djnz SS_lp
	ret

; HL=DE*A
Multi	ld b,8
	ld hl,0
Mulbouc	rra
	jr nc,Mulsaut
	add hl,de
Mulsaut	sla e
	rl d
	djnz Mulbouc
	ret

Disp_text
	ld a,(hl)
	or a
	ret z
	call #BB5A
	inc hl
	jr Disp_text

tempo	djnz $
	ret




op_offset
	db 0,1,2,8,9,10,#10,#11,#12


Okt1 equ %0010000000000000
Okt2 equ %0010010000000000
Okt3 equ %0010100000000000
Okt4 equ %0010110000000000
Okt5 equ %0011000000000000
Okt6 equ %0011010000000000
Okt7 equ %0011100000000000
Okt8 equ %0011110000000000


NoteTab dw 0 ;dummy note
	dw Okt1+343,Okt1+363,Okt1+385,Okt1+408,Okt1+432,Okt1+458
	dw Okt1+485,Okt1+514,Okt1+544,Okt1+577,Okt1+611,Okt1+647
	dw Okt2+343,Okt2+363,Okt2+385,Okt2+408,Okt2+432,Okt2+458
	dw Okt2+485,Okt2+514,Okt2+544,Okt2+577,Okt2+611,Okt2+647
	dw Okt3+343,Okt3+363,Okt3+385,Okt3+408,Okt3+432,Okt3+458
	dw Okt3+485,Okt3+514,Okt3+544,Okt3+577,Okt3+611,Okt3+647
	dw Okt4+343,Okt4+363,Okt4+385,Okt4+408,Okt4+432,Okt4+458
	dw Okt4+485,Okt4+514,Okt4+544,Okt4+577,Okt4+611,Okt4+647
	dw Okt5+343,Okt5+363,Okt5+385,Okt5+408,Okt5+432,Okt5+458
	dw Okt5+485,Okt5+514,Okt5+544,Okt5+577,Okt5+611,Okt5+647
	dw Okt6+343,Okt6+363,Okt6+385,Okt6+408,Okt6+432,Okt6+458
	dw Okt6+485,Okt6+514,Okt6+544,Okt6+577,Okt6+611,Okt6+647
	dw Okt7+343,Okt7+363,Okt7+385,Okt7+408,Okt7+432,Okt7+458
	dw Okt7+485,Okt7+514,Okt7+544,Okt7+577,Okt7+611,Okt7+647
	dw Okt8+343,Okt8+363,Okt8+385,Okt8+408,Okt8+432,Okt8+458
	dw Okt8+485,Okt8+514,Okt8+544,Okt8+577,Okt8+611,Okt8+647
	dw Okt8+647,Okt8+647,Okt8+647,Okt8+647  ; Pourquoi cette saturation ?
	dw Okt8+647,Okt8+647,Okt8+647,Okt8+647
	dw Okt8+647,Okt8+647,Okt8+647,Okt8+647
	dw Okt8+647,Okt8+647,Okt8+647,Okt8+647

channel_active  ds 9
channel_ins     ds 9
channel_note    ds 9
channel_note2   ds 9
channel_vol2    ds 9    ; connector
channel_vol     ds 9    ; carrier
channel_vpos    ds 9
channel_vspeed  ds 9
channel_vdepth  ds 9
channel_eff.l   ds 9
channel_eff.h   ds 9
channel_freq.l  ds 9
X
channel_freq.h  ds 9
channel_fshift.l ds 9
channel_fshift.h ds 9
channel_pdest.l ds 9
channel_pdest.h ds 9
channel_pspeed.l  ds 9
channel_pspeed.h  ds 9
channel_aspeed  ds 9
channel_acurpos ds 9
channel_acursp  ds 9
channel_vsubt   ds 9
channel_segment ds 9
;
Y
current_pos     ds 9
current_speed   ds 9,6
s_count	 	ds 9
current_line    ds 9
work_pattern    dw trackorder,trackorder,trackorder,trackorder,trackorder
		dw trackorder,trackorder,trackorder,trackorder
position_jump   ds 9
new_position    ds 9
break_pos       ds 9
pattern_break   ds 9
list
segments.l	ds 9
segments.h	ds 9	;!!! Mettre poids forts 9 octets apres poids faibles !!!
nolist
stopped	 	ds 9,1

;

mainvol	   db 64	   ; 64 = max, 0 = min
count	   db 0
Register_bypass ds 246

polling	 	db 1

segnum	  	dw 0
current_bpm     dw 0

; Variables a nous

variable

; Attention les valeurs chargees seront celles suivant 'defaut', ou fixees
; d'apres le header sadt.

delay	db 0 		; Meme role que current_speed, mais en global
d_count	db 0 		; equivalent du s_count 
current_line0 db 0
song_pos db 0

song_length db 0
song_loop db 0	

defaut
	db 8,0, 0,0

waitvbl
	ld b,#f5
	in a,(c)
	rra
	jr nc,waitvbl
	ret

antivbl
	ld b,#f5
	in a,(c)
	rra
	jr c,antivbl
	ret
