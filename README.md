Surprise! Adlib Tracker 2 player for Amstrad CPC
================================================

This player was originally developped by Madram for an Adlib compatible soundcard connected to the CPC-ISA.

This version is adjusted to work with the Willy adapter and the OPL3LPT soundcard.

Limitations:
- The replay speed is not correct
- Several effects are not implemented or inaccurate

The music will usually sound ok, but not the same as with the tracker or the PC player. Improvements welcome!

Usage
-----

Put the player (PLAY.BAS and A.OUT) on some mass storage (floppy, USB, ...) with some SA2 files.

RUN"PLAY.BAS

Select a file to play. Then enjoy the music.
